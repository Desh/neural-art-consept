nohup redis-server >> redis.log 2>&1 &
nohup sudo python3 ~/neural-art-consept/server_flask_init/run.py >> server.log 2>&1 &
nohup celery -A run.celery worker --loglevel=INFO --concurrency=1 >> celery.log 2>&1 &
