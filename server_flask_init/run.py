import os, sys, shlex, time
from subprocess import Popen, PIPE
from flask import Flask, request, jsonify, make_response, abort
from flask import url_for, redirect, render_template, send_from_directory
from werkzeug.utils import secure_filename
from celery import Celery
from config import *

app = Flask(__name__)


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER_IMG'] = UPLOAD_FOLDER_IMG
app.config['UPLOAD_FOLDER_STYLE'] = UPLOAD_FOLDER_STYLE
app.config['OUTPUT_FOLDER_IMG'] = OUTPUT_FOLDER_IMG
app.config['CELERY_BROKER_URL'] = REDIS_HOST_PORT
app.config['CELERY_RESULT_BACKEND'] = REDIS_HOST_PORT
app.config['CELERY_MAX_TASKS_PER_CHILD'] = 1


celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


images = [
    {
        'id': 0,
        'image': 'img_0.jpg',
        'style': 'style_0.jpg',
        'status': 'SUCCESS',
        'iterations': 0,
        'output': 'out_0.jpg'
    }
]

count_ready_img = 0


@app.route('/', methods=['GET'])
def show_landing_page():
    return render_template('index.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def set_filename(filename):
    expansion = secure_filename(filename).rsplit('.', 1)[1]
    return str(images[-1]['id'] + 1) + '.' + expansion


@app.route('/api/v1.0/tasks/revoke/<task_id>', methods=['DELETE'])
def revoke_task(task_id, image_id):
    celery.control.revoke(task_id=task_id)
    image_id = int(image_id)
    os.remove(os.path.join(app.config['UPLOAD_FOLDER_IMG'], images[image_id]['image']))
    os.remove(os.path.join(app.config['UPLOAD_FOLDER_STYLE'], images[image_id]['style']))
    return jsonify({'state': 'task was successfuly delete'})


@app.route('/api/v1.0/image/new/', methods=['POST'])
def post_new_image():
    print(request)
    print(request.files['file_img'])
    print(request.files['file_style'])
    print(request.form['iter'])
    iterations = request.form['iter']
    file_img = request.files['file_img']
    file_style = request.files['file_style']
    if (file_img and allowed_file(file_img.filename)) \
            and (file_style and allowed_file(file_style.filename)):

        filename_img = 'img_' + set_filename(file_img.filename)
        print(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
              filename_img))

        file_img.save(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
                                   filename_img))
        filename_style = 'style_' + set_filename(file_img.filename)
        print(file_style)
        file_style.save(os.path.join(app.config['UPLOAD_FOLDER_STYLE'],
                                     filename_style))
        count_ready_img = images[-1]['id'] + 1
        out_name = 'out_{}.jpg'.format(count_ready_img)
        images.append(
            {
                'id': images[-1]['id'] + 1,
                'image': filename_img,
                'style': filename_style,
                'iterations': iterations,
                'status': 'PREPARE',
                'output': out_name
            }
        )
        task = long_task.apply_async(args=[list(images[-1].items())])
        time.sleep(1)
        return redirect(url_for('check_status', task_id=task.id, image_id=images[-1]['id']), code=302)
    return jsonify(state='bad request', code=400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/status/<task_id>')
def check_status(task_id):
    task = long_task.AsyncResult(task_id)
    if request.args['image_id'] is not None:
        image_id = int(request.args['image_id'])
        print(image_id)
    else:
        return make_response(jsonify({'status': 'bad request'}), 400)
    if task.info is not None:
        response = {
            'image_id': image_id,
            'state': task.state,
            'iteration': task.info.get('iteration', 0),
            'whole':  task.info.get('whole', 0),
            'task_id': task_id,
            'output': images[image_id].get('output', '-')
        }
    else:
        response = {
            'image_id': image_id,
            'state': task.state,
            'progress': 'in queue',
            'task_id': task_id,
            'output': images[image_id].get('output', '-'),
            'iteration': -1
        }
    return jsonify(response)


@app.route('/download/image/<output_id>', methods=['GET'])
def download_ready_image(output_id):
    return send_from_directory(app.config['OUTPUT_FOLDER_IMG'], output_id)


@celery.task(bind=True)
def long_task(self, values):
    print(values)
    for key, value in values:
        if key == 'image':
            img_name = value
        elif key == 'style':
            style_name = value
        elif key == 'iterations':
            iterations = str(value)
        elif key == 'output':
            out_name = value
        elif key == 'id':
            image_id = value

    self.update_state(state='BEGIN', meta={'iteration': 0, 'whole': iterations})
    img_name = os.path.abspath(os.path.join(app.config['UPLOAD_FOLDER_IMG'], img_name))
    style_name = os.path.abspath(os.path.join(app.config['UPLOAD_FOLDER_STYLE'], style_name))
    out_name = os.path.abspath(os.path.join(app.config['OUTPUT_FOLDER_IMG'], out_name))
    print('python -u {} --iterations {} --subject {} --style {} --output {} '.format(NEURAL_SCRIPT_NAME,iterations, img_name, style_name, out_name))
    proc = Popen(shlex.split('python -u {} --iterations {} --subject {} --style {} --output {} --network {}'.format(NEURAL_SCRIPT_NAME,iterations, img_name, style_name, out_name, NETWORK)),
                 stdout=PIPE, stderr=PIPE)
    errors = proc.stderr.read()
    #if len(errors) > 0:
     #    print(errors)
      #   sys.stdout.flush()
    print('begin')
    for line in iter(p.stdout.readline, b''):
        print(">>> " + line.rstrip())
        self.update_state(state='PROGRESS', meta={'iteration': out, 'whole': iterations})
    proc.wait()
    return {'state': 'SUCCESS', 'whole': iterations, 'iteration': iterations, 'output': out_name}


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=80)

