UPLOAD_FOLDER_IMG = '/home/ubuntu/project/n_dp/input'
UPLOAD_FOLDER_STYLE = '/home/ubuntu/project/n_dp/styles'
OUTPUT_FOLDER_IMG = '/home/ubuntu/project/n_dp/images'

NETWORK = '/home/ubuntu/project/n_dp/imagenet-vgg-verydeep-19.mat'
LOCALHOST = 'http://0.0.0.0:80'
NEURAL_SCRIPT_NAME = '/home/ubuntu/project/n_dp/neural_artistic_style.py'
REDIS_HOST_PORT = 'redis://localhost:6379/0'

