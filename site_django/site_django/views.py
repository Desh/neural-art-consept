from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Image
from django.template import loader
from django.conf import settings
from django.utils.encoding import smart_str
import requests, os, shutil

SERVER_ADDRESS = 'http://127.0.0.1:5000'


def index(request):
    return render(request, 'index.html')


def upload(request):
    return render(request, 'upload.html')


def services(request):
    return render(request, 'base_test.html')


def login(request):
    return render(request, 'login.html')


def download(request, task_id, out_name):
    if os.path.isfile('{}/{}/{}'.format(settings.BASE_DIR, settings.OUTIMG_URL, out_name)):
            f = open('{}/{}/{}'.format(settings.BASE_DIR, settings.OUTIMG_URL, out_name), 'r')
            response = HttpResponse(f, content_type='image/jpeg')
            response['Content-Disposition'] = 'attachment; filename={}'.format(smart_str(out_name))
            return response
    else:
        response = requests.get('{}/download/image/{}'.format(SERVER_ADDRESS, out_name), stream=True)
        if response.status_code == 200:
            with open('{}/{}/{}'.format(settings.BASE_DIR, settings.OUTIMG_URL, out_name), 'wb') as f:
                shutil.copyfileobj(response.raw, f)
            f = open('{}/{}/{}'.format(settings.BASE_DIR, settings.OUTIMG_URL, out_name), 'r')
            response = HttpResponse(f, content_type='image/jpeg')
            response['Content-Disposition'] = 'attachment; filename={}'.format(smart_str(out_name))
            return response
        else:
            return Http404

def check_user(request):
    print(request.GET.get('Email'))
    print(request.GET.get('Password'))
    return render(request, 'index.html')


def show_status(request, image_id, task_id):
    re = requests.get('{}/status/{}'.format(SERVER_ADDRESS, task_id),
                      params={'image_id': image_id})
    template = loader.get_template('show_status.html')
    print(re.json())
    context = re.json()
    return HttpResponse(template.render(context, request))


def ajax_get_status(request, image_id, task_id):
    response = requests.get('{}/status/{}'.format(SERVER_ADDRESS, task_id),
                  params={'image_id': image_id})
    if response.status_code == 200:
        return HttpResponse(response)
    else:
        return Http404



@csrf_exempt
def create_new(request):
    if request.method == ['GET']:
        return render('upload.html')
    post_data = [('iter', request.POST.get('iter'))]
    post_files = [('file_img', request.FILES.get('file_img')),
                  ('file_style', request.FILES.get('file_style'))]
    new_request = requests.post(SERVER_ADDRESS + '/api/v1.0/image/new/', data=post_data,
                                files=post_files)
    print(new_request.json())
    image = Image()
    new_request = new_request.json()
    image.task_id = new_request['task_id']
    image.output = new_request['output']
    image.state = new_request['state']
    image.iteration = new_request.get('iteration', -1)
    # /status/ require image_id param in request

    return redirect('show_status/{}/{}'.format(new_request['image_id'], new_request['task_id']))
    # return render(request, 'show_status.html', context)
    # return HttpResponse(template.render(context, request))