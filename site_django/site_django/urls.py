"""untitled1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from site_django.views import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^upload/', upload),
    url(r'^services/', services),
    url(r'^login/', login),
    url(r'^check_user/', check_user),
    url(r'^create_new', create_new),
    url(r'^show_status/(?P<image_id>[0-9]+)/(?P<task_id>.+)/$', show_status),
    url(r'^download/(?P<task_id>.+)/(?P<out_name>.+)', download),
    url(r'^ajax_status/(?P<image_id>[0-9]+)/(?P<task_id>.+)/$', ajax_get_status)
]
