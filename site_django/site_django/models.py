from django.db import models


class Image(models.Model):
    task_id = models.CharField(max_length=30)
    output = models.CharField(max_length=30)
    state = models.CharField(max_length=30)
    iteration = models.CharField(max_length=30)